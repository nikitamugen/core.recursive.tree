package recoursive.app;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import recoursive.tree.TreeBuilder;

public class ItemTreeBuilder implements TreeBuilder<Item> {

	private List<Item> items;

	ItemTreeBuilder() {
		this.items = new ArrayList<>();
	}

	@Override
	public void add(Item item) {
		items.add(item);
	}

	@Override
	public ItemNode build() {
		return items
				.stream()
				.sorted(Comparator.comparing(Item::getNumber))
				.map(ItemNode::new)
				.reduce((root, node) -> (ItemNode)root.Push(node))
				.get();
	}

}
