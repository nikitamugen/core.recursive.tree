package recoursive.app;

import recoursive.tree.Node;
import recoursive.tree.TreeBuilder;

public class App {

	public static void main(String[] args) {
		
		// items must be sorted by level
		// from first to last
		//
		TreeBuilder<Item> tree = new ItemTreeBuilder();
		
		tree.add(new Item("1", "1"));
		tree.add(new Item("0", "0"));
		tree.add(new Item("1.1", "11"));
		tree.add(new Item("1.2", "12"));
		tree.add(new Item("1.3", "13"));
		tree.add(new Item("1.3.1", "131"));
		tree.add(new Item("1.1.1", "111"));
		tree.add(new Item("1.2.1", "121"));
		tree.add(new Item("1.2.2", "122"));
		tree.add(new Item("1.3.1.1", "131"));
		tree.add(new Item("1.7.7.7", "1777"));
		
		Node<Item> root = tree.build();
		
		root.print("");
		
		System.out.println("Tree plain:");
		root.exec(data -> System.out.println("data number: " + data.number));
	}

}
