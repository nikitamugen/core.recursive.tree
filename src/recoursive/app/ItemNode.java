package recoursive.app;

import java.util.function.Consumer;

import recoursive.tree.EmptyNode;
import recoursive.tree.Node;

public class ItemNode implements Node<Item> {

	private Item data;
	private Node<Item> child;
	private Node<Item> brother;
	
	private Node<Item> parent;
	
	ItemNode(Item data) {
		this.data = data;
		this.child = new EmptyNode<>();
		this.brother = new EmptyNode<>();
		this.parent = new EmptyNode<>();
	}
	
	@Override
	public Item getData() {
		return data;
	}

	@Override
	public Node<Item> getChild() {
		return child;
	}

	@Override
	public Node<Item> getBrother() {
		return brother;
	}
	
	@Override
	public Node<Item> getParent() {
		return parent;
	}

	@Override
	public void setParent(Node<Item> parent) {
		this.parent = parent;
		
		brother.setParent(parent);
	}

	@Override
	public Node<Item> Push(Node<Item> node) {
		if (node.isEmpty()) {
			return this;
		}
		
		final String thisNodeNumber = this.getData().number;
		final String guestNodeNumber = node.getData().number;
		
		if (guestNodeNumber.startsWith(thisNodeNumber)
				|| thisNodeNumber.equals("0")) {
			child = child.Push(node);
			child.setParent(this);
			return this;
		}
		
		if (thisNodeNumber.startsWith(guestNodeNumber)
				|| guestNodeNumber.equals("0")) {
			node.Push(this);
			setParent(node);
			return node;
		} 
		
		brother = brother.Push(node);
		return this;
	}

	@Override
	public Boolean isEmpty() {
		return Boolean.FALSE;
	}
	
	@Override
	public void print(String prefix) {
		if (this.parent.isEmpty()) {
			System.out.println(prefix + data.number + " | " + data.name);
		} else {
			System.out.println(prefix + data.number + " | " + data.name + " | parent: " + this.parent.getData().number);
		}
		
		child.print(prefix + "---");
		brother.print(prefix);
	}

	@Override
	public void exec(Consumer<Item> some) {
		some.accept(data);
		
		child.exec(some);
		brother.exec(some);
	}

}
