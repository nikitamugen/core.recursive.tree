package recoursive.app;

public class Item {
	public String number;
	public String name;
	
	public String getNumber() {
		return number;
	}
	
	Item(String number) {
		this.number = number;
		this.name = null;
	}
	
	Item(String number, String name) {
		this.number = number;
		this.name = name;
	}
}
