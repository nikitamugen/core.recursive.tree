package recoursive.util;

public class Strings {
	public static int countChar(String string, char find) {
		int counter = 0;
		for( int i=0; i<string.length(); i++ ) {
		    if( string.charAt(i) == find ) {
		        counter++;
		    } 
		}
		return counter;
	}
	
	public static String piece(String string, char delimiter, int count) {
		if (count > 0) {
			return pieceForward(string, delimiter, count);
		} else if (count < 0) {
			return pieceBackward(string, delimiter, Math.abs(count));
		}
		
		return string;
	}
	
	private static String pieceForward(String string, char delimiter, int count) {
		int counter = 0;
		int length = string.length();
		int i = 0;
		for( ; i<length && counter < count; i++ ) {
		    if( string.charAt(i) == delimiter ) {
		        counter++;
		    } 
		}
		
		if (i == length) {
			return string;
		}
		return string.substring(0,i-1);
	}
	
	private static String pieceBackward(String string, char delimiter, int count) {
		int counter = 0;
		int length = string.length();
		int i = length-1;
		for( ; i>=0 && counter < count; i-- ) {
		    if( string.charAt(i) == delimiter ) {
		        counter++;
		    } 
		}
		
		if (i == length) {
			return string;
		}
		return string.substring(0,i+1);
	}
}
