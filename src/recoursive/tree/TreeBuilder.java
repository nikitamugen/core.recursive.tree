package recoursive.tree;

public interface TreeBuilder<T> {
	public void add(T item);
	public Node<T> build();
}
