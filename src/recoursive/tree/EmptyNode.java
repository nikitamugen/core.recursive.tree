package recoursive.tree;

import java.util.function.Consumer;

public class EmptyNode<T> implements Node<T> {
	
	private Node<T> parent;
	
	@Override
	public Boolean isEmpty() {
		return Boolean.TRUE;
	}

	@Override
	public Node<T> Push(Node<T> node) {
		return node.Push(this);
	}
	
	@Override
	public T getData() {
		throw new RuntimeException("Empty node cann't have data");
	}
	
	@Override
	public Node<T> getChild() {
		throw new RuntimeException("Empty node cann't have child");
	}

	@Override
	public Node<T> getBrother() {
		throw new RuntimeException("Empty node cann't have brother");
	}

	@Override
	public void print(String prefix) {
	}
	
	@Override
	public void exec(Consumer<T> some) {
	}

	@Override
	public Node<T> getParent() {
		return parent;
	}

	@Override
	public void setParent(Node<T> parent) {
		this.parent = parent;
	}

}
