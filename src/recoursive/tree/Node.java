package recoursive.tree;

import java.util.function.Consumer;

public interface Node<T> {
	public T getData();
	
	public Node<T> getChild();
	
	public Node<T> getBrother();
	
	public Node<T> getParent();
	public void setParent(Node<T> parent);
	
	public Boolean isEmpty();
	
	public Node<T> Push(Node<T> node);
	
	public void print(String prefix);
	
	public void exec(Consumer<T> some);
}
